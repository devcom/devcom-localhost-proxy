# Localhost Proxies

This repo provides the ability to use various proxies for local development with trusted certs provided by Lets Encrypt. 

> **This repo is currently using the _localhost.devcom.vt.edu_ space, but we hope to gain access to _localhost.vt.edu_ soon. This is to help us get started.**

## Why? What's the point?

There are a few reasons we would like to have proxies for localhost. A few ideas might be:

- **More closely mimics prod environments.** Most of our prod environments perform TLS termination at a load balancer or some other proxy before the app gets the traffic.
- **Fewer config differences.** Many apps validate that traffic was received over TLS (either directly or via a proxy). Instead of disabling that check in local dev and hoping it's turned on in non-dev environments, we can always have it on!
- **Service-to-service communication can occur over HTTPS.** For some services (like validating a CAS ticket), some traffic _has_ to travel over HTTPS. 
- **No need to configure/rotate self-signed certs.** With a trusted cert, you don't need to configure your browser or each individual app to trust a self-signed cert.


## Available Proxies

The following proxies are available for you to use (they each have their own README with instructions). If you want to see another proxy, make an issue and let us know! Pull requests are welcome too!

- [Traefik 1.7](./proxies/traefik-1.7)
- [Traefik 2.3](./proxies/traefik-2.3)


## Certificate Information

The current cert is stored in the `/cert` directory.

- **Cert names** - the certificates are generated using both the **localhost.devcom.vt.edu** and ***.localhost.devcom.vt.edu** names
- **Cert rotation** - certs are automatically rotated monthly by using a scheduled GitLab pipeline
- **Cert provisioning** - certs are provisioned using Lets Encrypt using [certbot](https://certbot.eff.org/) with the [Route53 plugin](https://github.com/certbot/certbot/tree/master/certbot-dns-route53) as part of the [GitLab build pipeline definition](./.gitlab-ci.yml).

### DNS Structure

To ensure least privilege for our build pipelines, the following DNS structure is currently being used. This allows the AWS user used to update Route53 to have access to only the zone necessary for cert validation. Any compromise of tokens will not affect A/AAAA records for localhost.devcom.vt.edu. All Terraform used to setup the environment can be found in the [/tf folder](./tf).

```mermaid
graph LR;
  vt[(vt.edu)] -- NS records --> devcom[(devcom.vt.edu)]
  devcom -- NS records --> acme[(_acme-challenge.localhost.devcom.vt.edu)]
  devcom ---| A/AAAA records | localhostWildcard[*.localhost.devcom.vt.edu]
  devcom ---| A/AAAA records | localhost[localhost.devcom.vt.edu]
  certbot[/GitLab Pipeline/] -- creates TXT records --> acme
```

## Detecting Cloud Architecture Drift

COMING SOON - To ensure the Terraform in the repo is what is actually deployed, a read-only IAM user is utilized. On a scheduled interval, a `terraform plan` is executed. By including the `-detailed-exitcode` flag, if there is anything to apply, the plan will fail. If the plan fails, the job will fail, causing Slack messages to be send to #devcom-ops.
