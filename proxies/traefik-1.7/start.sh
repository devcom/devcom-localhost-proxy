#!/bin/sh

set -e

# Clone the repo if not already here (in the case of restarting an existing container)
if [ ! -d "/etc/local-ssl" ]; then
  mkdir -p /etc/local-ssl
  cd /etc/local-ssl

  git clone --depth=1 https://code.vt.edu/devcom/devcom-localhost-proxy.git .
else
  cd /etc/local-ssl
  git pull
fi

# Invoke the default entrypoint of the Traefik image
exec "/entrypoint.sh" "$@"
