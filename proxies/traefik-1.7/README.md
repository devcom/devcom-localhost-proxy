# Traefik 1.7

This proxy utilizes the [Traefik v1.7](https://hub.docker.com/_/traefik) image and auto-configures the trusted localhost cert as the default certificate.

## Example Usage

The following Docker Compose file provides a sample of running the proxy.

```yaml
version: "3.7"

services:
  proxy:
    image: code.vt.edu:5005/devcom/devcom-localhost-proxy:traefik-1.7
    ports:
      - 80:80
      - 443:443
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      default:
        aliases: # Ensure all container-to-container HTTP requests go through the proxy
          - nginx.localhost.devcom.vt.edu
          - nginx2.localhost.devcom.vt.edu
  app:
    image: nginx:alpine
    labels:
      traefik.backend: nginx
      traefik.frontend.rule: Host:nginx.localhost.devcom.vt.edu
  app2:
    image: nginx:alpine
    labels:
      traefik.backend: nginx2
      traefik.frontend.rule: Host:nginx2.localhost.devcom.vt.edu
```

## Exposing the Traefik Dashboard

If you want to expose the Traefik dashboard (or make any other configuration changes), you can specify any additional flags through the `command` (just as you would do with a plain Traefik container).

In this case, we are exposing the Traefik dashboard and allowing it to be accessible only on the local machine.

```yml
services:
  proxy:
    image: code.vt.edu:5005/devcom/devcom-localhost:traefik-1.7
    command: --api
    ports:
      - 80:80
      - 443:443
      - 127.0.0.1:8080:8080
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    ...
```
