# Traefik 2.3

This proxy utilizes the [Traefik v2.3](https://hub.docker.com/_/traefik) image and auto-configures the trusted localhost cert as the default certificate.

## Example Usage

The following Docker Compose file provides a sample of running the proxy.

```yaml
version: "3.7"

services:
  proxy:
    image: code.vt.edu:5005/devcom/devcom-localhost-proxy:traefik-2.3
    ports:
      - 80:80
      - 443:443
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      default:
        aliases: # Ensure all container-to-container HTTP requests go through the proxy
          - nginx.localhost.devcom.vt.edu
          - nginx2.localhost.devcom.vt.edu
  app:
    image: nginx:alpine
    labels:
      traefik.http.routers.nginx.rule: Host(`nginx.localhost.devcom.vt.edu`)
  app2:
    image: nginx:alpine
    labels:
      traefik.http.routers.nginx2.rule: Host(`nginx2.localhost.devcom.vt.edu`)
      # Use the following label if your app is running on a different port
      # traefik.http.services.nginx2.loadbalancer.server.port: 3000
```

## Accessing the Traefik Dashboard

The Traefik dashboard is already enabled by default, so only needs to be exposed. Due to its capabilities, it's best to expose it only on the local machine, rather than on the entire network.

```yml
services:
  proxy:
    image: code.vt.edu:5005/devcom/devcom-localhost:traefik-2.3
    ports:
      - 80:80
      - 443:443
      - 127.0.0.1:8080:8080
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    ...
```
