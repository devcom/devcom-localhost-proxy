#!/bin/sh

set -e

if [ ! -d "/etc/local-ssl" ]; then
  echo "Cloning localhost certificate repo"

  mkdir -p /etc/local-ssl
  cd /etc/local-ssl

  git clone --depth=1 https://code.vt.edu/devcom/devcom-localhost-proxy.git .
else
  echo "Pulling latest certificate from cert repo"

  cd /etc/local-ssl
  git pull
fi

if [ -f "/etc/traefik/traefik-extension.toml" ]; then
  echo "Merging in traefik-extension.toml file into traefik.toml"
  cat /etc/traefik/traefik-extension.toml >> /etc/traefik/traefik.toml
fi

echo "Starting with $@"

exec "/entrypoint.sh" "--api"
