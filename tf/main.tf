terraform {
  required_version = ">= 0.12"

  backend "s3" {
    bucket = "devcom-terraform-state"
    key    = "localhost-devcom/state.json"
    region = "us-east-1"
  }
}

provider "aws" {
  version = "2.61.0"
  region  = "us-east-1"
}

locals {
  default_tags = {
    Service           = "localhost-devcom"
    Environment       = "production" # Even though it's supporting local development
    ResponsibleParty  = "mikesir"
    ResponsibleParty2 = "none"
    DataRisk          = "low"
    ComplianceRisk    = "none"
    Documentation     = "none"
    Version           = "v1"
    VCS               = "https://code.vt.edu/devcom/devcom-localhost-proxy"
  }
}


###################################################
#                   DNS SETUP                     #
###################################################

data "aws_route53_zone" "devcom_root" {
  name = "devcom.vt.edu."
}

resource "aws_route53_record" "localhost_root" {
  zone_id = data.aws_route53_zone.devcom_root.zone_id
  name    = "localhost.devcom.vt.edu"
  type    = "A"
  ttl     = 86400
  records = ["127.0.0.1"]
}

resource "aws_route53_record" "localhost_root_v6" {
  zone_id = data.aws_route53_zone.devcom_root.zone_id
  name    = "localhost.devcom.vt.edu"
  type    = "AAAA"
  ttl     = 86400
  records = ["::1"]
}

resource "aws_route53_record" "localhost_wildcard" {
  zone_id = data.aws_route53_zone.devcom_root.zone_id
  name    = "*.localhost.devcom.vt.edu"
  type    = "A"
  ttl     = 86400
  records = ["127.0.0.1"]
}

resource "aws_route53_record" "localhost_wildcard_v6" {
  zone_id = data.aws_route53_zone.devcom_root.zone_id
  name    = "*.localhost.devcom.vt.edu"
  type    = "AAAA"
  ttl     = 86400
  records = ["::1"]
}

resource "aws_route53_zone" "devcom_cert_validation" {
  name = "_acme-challenge.localhost.devcom.vt.edu"
  tags = local.default_tags
}

resource "aws_route53_record" "localhost_ns" {
  zone_id = data.aws_route53_zone.devcom_root.zone_id
  name    = "_acme-challenge.localhost.devcom.vt.edu"
  type    = "NS"
  ttl     = "30"

  records = [
    aws_route53_zone.devcom_cert_validation.name_servers.0,
    aws_route53_zone.devcom_cert_validation.name_servers.1,
    aws_route53_zone.devcom_cert_validation.name_servers.2,
    aws_route53_zone.devcom_cert_validation.name_servers.3,
  ]
}


###################################################
#    IAM USER TO CREATE CERT VALIDATION RECORDS   #
###################################################

resource "aws_iam_user" "cert_creator" {
  name = "ci-localhost-cert-creator"
  tags = local.default_tags
}

resource "aws_iam_user_policy" "cert_creator" {
  name = "dns-update-policy"
  user = aws_iam_user.cert_creator.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Id": "certbot-update-policy",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "route53:ListHostedZones",
                "route53:GetChange"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:ChangeResourceRecordSets"
            ],
            "Resource": [
                "arn:aws:route53:::hostedzone/${aws_route53_zone.devcom_cert_validation.zone_id}"
            ]
        }
    ]
}
EOF
}
